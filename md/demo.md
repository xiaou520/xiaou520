@[toc](目录)

# demo
## 这是是一个demo

```js
console.log("hello world")
```
## 任务列表

- [x] 已完成任务
- [ ] 未完成任务


## $\KaTeX$公式

我们可以渲染公式例如：$x_i + y_i = z_i$和$\sum_{i=1}^n a_i=0$
我们也可以单行渲染
$$\sum_{i=1}^n a_i=0$$

## mermaid
```mermaid
graph LR;  
　　A-->B;
　　A-->C;
　　B-->D;
　　C-->D;
```
## Graphviz
```graphviz
digraph G {
    A -> B;
    A -> C -> B;
    A -> D -> B;
}
```
## flowchart
```flow
start=>start: 开始
end=>end: 结束
operation=>operation: 我的操作
start->operation->end
```
## echarts
```echarts
{"xAxis":{"type":"category","data":["Mon","Tue","Wed","Thu","Fri","Sat","Sun"]},"yAxis":{"type":"value"},"series":[{"data":[820,932,901,934,1290,1330,1320],"type":"line"}]}
```
## abbr
*[HTML]: Hyper Text Markup Language
*[W3C]:  World Wide Web Consortium

HTML 规范由 W3C 维护
```
*[HTML]: Hyper Text Markup Language
*[W3C]:  World Wide Web Consortium
HTML 规范由 W3C 维护
```
## 脚注
见底部脚注[^hello]

[^hello]: 一个注脚